<?php
require_once __DIR__ . '/includes/Controller.class.php';
require_once __DIR__ . '/includes/View.class.php';

$controller = new Controller();
$view = new View();


try {
    $action = $_REQUEST['action'] ?? 'index';

    switch ($action) {
        case 'delete':
            $controller->actionDelete();
            break;
        case 'index':
            $controller->actionIndex();
            break;
        case 'upload':
            $controller->actionUpload();
            break;
        case 'view_results':
            $controller->actionViewResult();
            break;
        case 'main_page':
            $controller->actionMainPage();
            break;
        default:
            throw new Exception("Неизвестный параметр action: " . var_export($action, true));
            break;
    }
} catch (Exception $e) {
    $view->echoErrorPage($e);
}
