<?php
require_once __DIR__ . '/db/DbDecorator.class.php';

class Model
{
    private $db;
    private $tableName = 'test_data';

    public function __construct()
    {
        $this->db = DbDecorator::getInstance();
    }

    public function delete()
    {
        $sql = "DROP TABLE IF EXISTS {$this->tableName}";
        $this->db->exec($sql);

        $sql = "CREATE TABLE {$this->tableName}(
    `UID` INT,
    `Name` VARCHAR(24),
    `Age` INT,
    `Email` VARCHAR(30),
    `Phone` VARCHAR (20),
    `Gender` VARCHAR(6)
)";
        $this->db->exec($sql);
    }

    public function upload()
    {
        if ($_FILES['uploadfile']['type'] !== "text/csv") {
            throw new Exception("Ошибка при загрузке файла. Файл должен быть в формате .CSV");
        }

        if ($_FILES['uploadfile']['error'] == 2) {
            throw new Exception("Ошибка при загрузке файла. Размер файла больше 1Mb");
        } elseif (in_array($_FILES['uploadfile']['error'], range(3, 8))) {
            throw new Exception("Ошибка при загрузке файла.");
        }

        $this->workWithUploadedFile($_FILES['uploadfile']['tmp_name']);
    }

    private function workWithUploadedFile(string $filePath)
    {
        $fileRows = file($filePath);
        if ($fileRows[0] != "UID,Name,Age,Email,Phone,Gender\n") {
            throw new Exception("Некорректная структура файла");
        }
        // В $fileRows[0] лежат названия колонок таблицы из БД, поэтому $fileRows[0] удаляем
        unset($fileRows[0]);


        foreach ($fileRows as $fileRow) {
            $arrayRow = explode(',', $fileRow);
            $uid = $arrayRow[0];

            foreach ($arrayRow as $key => $value) {
                // Обрезаем лишние пробелы, ньюлайны и тд в значениях
                $arrayRow[$key] = trim($arrayRow[$key]);
                $arrayRow[$key] = $this->db->quote($arrayRow[$key]);
            }
            if(in_array($uid, $this->db->fetchCol("SELECT UID FROM test_data")))
            {
                $sql = "UPDATE test_data SET `Name`= {$arrayRow[1]}, Age = {$arrayRow[2]}, Email = {$arrayRow[3]}, Phone = {$arrayRow[4]}, Gender = {$arrayRow[5]} WHERE UID = {$uid}";
                $this->db->exec($sql);
            }
            else {
                $arrayRow = join(", ", $arrayRow);
                $sql = "INSERT INTO {$this->tableName} VALUES ({$arrayRow})";
                $this->db->exec($sql);
            }
        }
    }

    private function addTextFilter(array &$sqlWhere, string $paramName)
    {
        $value = $_REQUEST[$paramName] ?? '';
        if ($value !== '') {
            $quotedValue = $this->db->quote("%{$value}%");
            $sqlWhere[] = "{$paramName} LIKE {$quotedValue}";
        }
    }

    private function addSelectFilter(array &$sqlWhere, string $paramName)
    {
        $value = $_REQUEST[$paramName] ?? '';
        if ($value !== '') {
            $quotedValue = $this->db->quote("{$value}");
            $sqlWhere[] = "{$paramName} = {$quotedValue}";
        }
    }

    private function addCompareFilter(array &$sqlWhere, string $paramName, string $sqlParamName, string $sign)
    {
        $value = $_REQUEST[$paramName] ?? '';
        if ($value !== '') {
            $value = (int)$value;
            $sqlWhere[] = "{$sqlParamName} {$sign} {$value}";
        }
    }

    public function getArrFromDb()
    {
        $sqlWhere = ['TRUE'];

        foreach ([
                     'Name',
                     'Email',
                     'Phone',
                 ] as $paramName) {
            $this->addTextFilter($sqlWhere, $paramName);
        }

        $this->addSelectFilter($sqlWhere, 'Gender');

        $this->addCompareFilter($sqlWhere, 'age_from', 'Age', '>=');
        $this->addCompareFilter($sqlWhere, 'age_to', 'Age', '<=');

        $orderBy = '';
        if (isset($_REQUEST['sort'])) {
            $orderBy = "ORDER BY {$_REQUEST['sort']} {$_REQUEST['order']}";
        }

        $sql = "SELECT * FROM {$this->tableName} WHERE " . join(' AND ', $sqlWhere) . " {$orderBy}";
        $result = $this->db->fetchAll($sql);

        if(empty($result))
        {
            throw new Exception("Отсутствуют данные");
        }

        return $result;
    }
}