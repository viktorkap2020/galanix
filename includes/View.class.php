<?php
require_once __DIR__ . '/Model.class.php';

class View
{
    public $arr;

    public function echoPage()
    {
        echo $this->mainPage();
    }

    public function echoErrorPage(Exception $e)
    {
        $traceString = "Trace:<br>";
        foreach ($e->getTrace() as $item) {
            $args = $item['args'] ?? [];
            $argsString = '';
            foreach ($args as $arg) {
                $argsString .= var_export($arg, true) . ', ';
            }
            $traceString .= "{$item['file']}:{$item['line']} function {$item['function']}({$argsString})";

            $traceString .= "<br>";
        }

        $str = "
    <html lang='ru'>
<head>
</head>
<body>
    <h1>Ошибка</h1>
    <p style='color: red'>{$e->getMessage()}</p>
    {$traceString}
    <p>GET: <pre>" . var_export($_GET, true) . "</pre></p>    
    <p>POST: <pre>" . var_export($_POST, true) . "</pre></p>    
</body>
</html>";
        die($str);
    }


    public function getTable(): string
    {
        $result = '';

        $order = (($_REQUEST['order'] ?? '') == 'asc')
            ? 'desc'
            : 'asc';

        $result .= "
<table class=\"table\">
  <thead class=\"thead-dark\"> 
      <tr>
      <th>UID</th>
        <th>
            <a href='/index.php?action=view_results&sort=name&order={$order}'>Name</a>
            <input type='text' form='filter_form' name='Name' placeholder='Имя'>
        </th>
        <th>
            Age
            <input type='text' form='filter_form' name='age_from' placeholder='Возраст от'>
            <input type='text' form='filter_form' name='age_to' placeholder='Возраст до'>
        </th>
        <th>
            Email
            <input type='text' form='filter_form' name='Email' placeholder='Электронная почта'>
        </th>
        <th>
            Phone
            <input type='text' form='filter_form' name='Phone' placeholder='Номер телефона'>
        </th>
        <th>
            Gender
            <select form='filter_form' name='Gender'>
                <option selected value=''>all</option>
                <option value='male'>male</option>
                <option value='female'>female</option>
            </select>
            </th>
    </tr>
    </thead>";

        foreach ($this->arr as $value) {
            $result .= "<tr style='background-color: rgb(230, 230, 230)'>";
            $result .= "<td>{$value['UID']}</td>";
            $result .= "<td>{$value['Name']}</td>";
            $result .= "<td>{$value['Age']}</td>";
            $result .= "<td>{$value['Email']}</td>";
            $result .= "<td>{$value['Phone']}</td>";
            $result .= "<td>{$value['Gender']}</td>";
            $result .= "</tr>";
        }
        $result .= "</table>";

        return $result;
    }

    public function viewResult()
    {
        echo "
        <html>
<head>
    <link rel = 'stylesheet' href = 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
Переход к загрузке данных:
<a href = '/index.php?action=main_page' class = 'badge badge-primary'>Import Data</a>
{$this->getFilterForm()}
{$this->getTable()}

</body>
</html>
";
    }

    public function mainPage()
    {
        return "<html>
<head>
    <!-- Подключил Bootstrap-->
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
Загрузить возможно файл ТОЛЬКО в формате .CSV и размером НЕ БОЛЬШЕ 1 MB !!!
<form action='/?action=upload' method='post' enctype='multipart/form-data'>
    <!-- Проверяем размер файла(если больше 1Мб -- не загружаем)-->
    <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
    <input type='file' name='uploadfile'>
    <input type='submit' value='Import' class='btn btn-primary'>
</form>

Очистить все данные:
<a href='/?action=delete' class='btn btn-outline-danger'>Clear all records</a>
<br>
<br> Переход к таблице данных:
<a href='/index.php?action=view_results' class='badge badge-secondary'>View Results</a>

</body>";
    }

    public function getFilterForm()
    {
        return "
        <form action='' id='filter_form'>
            <input type='hidden' name='action' value='{$_REQUEST['action']}'>
            <input type='submit' value='Применить фильтры'>
        </form>
        ";
    }
}