<?php
require_once __DIR__ . '/Model.class.php';
require_once __DIR__ . '/View.class.php';
class Controller
{
    private $model;
    private $view;

    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View();
    }

    public function actionIndex()
    {
        $this->view->echoPage();
    }

    public function actionDelete()
    {
        $this->model->delete();
        $this->actionIndex();
    }

    public function actionUpload()
    {
        $this->model->upload();
        $this->actionIndex();
    }

    public function actionViewResult()
    {
        $this->view->arr = $this->model->getArrFromDb();
        $this->view->viewResult();
    }

    public function actionMainPage()
    {
        $this->view->mainPage();
        $this->actionIndex();
    }
}